# Install python libraries
echo "Install python libraries"
pip install tensorflow-gpu
pip install opencv-python
pip install Luminoth

# Check luminoth
echo "Check luminoth"
lumi checkpoint refresh
lumi checkpoint list

# Convert xml to dataframe
echo "Convert PascalVOC to dataframe"
python PascalVOC_to_dataframe.py

# Unzip images
echo "Unzip files"
unzip original_data/dataset/images.zip -d original_data/dataset/

# Process images
echo "Process images"
python preprocess_image_transformations.py

# Convert image to TF records
echo "Convert image to TF records"
lumi dataset transform --type csv --data-dir data/ --output-dir tfdata/ --split train --split val  --only-classes table

# Start training
echo "Start training"
lumi train -c config.yml
