import os
from luminoth import Detector, read_image, vis_objects

max_height = 2339
max_width = 1654


def objects_to_dicts(filename, objects, threshold=None, smart_adjust=False):
    if isinstance(threshold, float):
        objects = [object for object in objects if object['prob'] > threshold]
    xmin, ymin, xmax, ymax = object['bbox']
    if smart_adjust:
        width_ratio = (xmax - xmin) / max_width
        height_ratio = (ymax - ymin) / max_height
        if width_ratio > 0.7:
            xmin = 0
            xmax = max_width
        if height_ratio < 0.8:
            ymax = max_height
            ymin = 0
        # todo the case of half table
    return [ {  'filename': filename,
                'prob': object['prob'], 'label': 'table_dl_{}'.format(str(object['prob'])), 'xmin': object['bbox'][0],
                'ymin':object['bbox'][1], 'xmax': object['bbox'][2], 'ymax': object['bbox'][3]} for object in objects ]


def objects_to_xml(filename, objects, threshold=None, smart_adjust=False):
    objects_ = ""
    if isinstance(threshold, float):
        objects = [object for object in objects if object['prob'] > threshold]

    xmin, ymin, xmax,ymax = object['bbox']
    if smart_adjust:
        width_ratio = (xmax - xmin) / max_width
        height_ratio = (ymax - ymin) / max_height
        if width_ratio > 0.7:
            xmin = 0
            xmax = max_width
        if height_ratio < 0.8:
            ymax = max_height
            ymin = 0
        # todo the case of half table
    for object in objects:
        objects_ += """
            <object>
                    <name>table_dl_{}</name>
                    <pose>Unspecified</pose>
                    <truncated>0</truncated>
                    <difficult>0</difficult>
                    <bndbox>
                        <xmin>{}</xmin>
                        <ymin>{}</ymin>
                        <xmax>{}</xmax>
                        <ymax>{}</ymax>
                    </bndbox>
                </object>
        """.format(str(object['prob']), str(object['bbox'][0]), str(object['bbox'][1]), str(object['bbox'][2]), str(object['bbox'][3]))

    return """
        <annotation>
            <folder>sg_ddr_2017</folder>
            <filename>{}</filename>
            <path>C:\\Users\\hamza\\Desktop\\komeos\\sg_ddr_2017\\{}</path>
            <source>
                <database>Unknown</database>
            </source>
            <size>
                <width>1654</width>
                <height>2339</height>
                <depth>3</depth>
            </size>
            <segmented>0</segmented>
            {}
        </annotation>
        """.format(filename,filename, objects_)


if __name__ == '__main__':
    checkpoint = input("Enter checkpoint id : ")
    detector = Detector(checkpoint=checkpoint)
    directory = "predictions/transformed_2018"
    val_files = [os.path.join(directory, f) for f in os.listdir(directory) if os.path.isfile( os.path.join(directory, f)) and f.endswith('.jpg')]
    for filename in val_files:
        image = read_image(filename)
        try:
            objects = detector.predict(image)
            if objects:
                xml = objects_to_xml(filename, objects, threshold=0.7)
                with open("predictions\\labels\\{}".format(filename.replace('.jpg', '.xml')), 'w') as f:
                    f.write(xml)
        except:
            print('Error in file: {}'.format(filename))