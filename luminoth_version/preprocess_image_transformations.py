import os
import cv2
import pandas as pd

def main(df_list, image_target_dir, resize=None):

    for df_path in df_list:
        df = pd.read_csv(df_path)
        print("Dataframe: {} has {} images".format(df_path,len(df)))
        filenames = df.image_id.tolist()
        directories = df.directory_images.tolist()
        if 'train' in df_path:
            dataset_type = 'train'
        elif 'val' in df_path:
            dataset_type = 'val'
        else:
            dataset_type = None

        for index, img in enumerate(filenames):
            path = os.path.join(directories[index], img)
            print(path)
            image = cv2.imread(path)
            if resize:
                image = cv2.resize(image, resize)

            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # perform transformations on image
            b = cv2.distanceTransform(image, distanceType=cv2.DIST_L2, maskSize=5)
            g = cv2.distanceTransform(image, distanceType=cv2.DIST_L1, maskSize=5)
            r = cv2.distanceTransform(image, distanceType=cv2.DIST_C, maskSize=5)

            transformed_image = cv2.merge((b, g, r))
            if dataset_type:
                target_file = os.path.join( os.path.join( image_target_dir, dataset_type) , img)
            else:
                target_file = os.path.join(image_target_dir, img)
            cv2.imwrite(target_file, transformed_image)


def simple_transform(dir_path, image_target_dir):
    original_files = [ f for f in os.listdir(dir_path) if os.path.isfile( os.path.join(dir_path, f)) and f.endswith('.jpg')]
    print("Dir: {} has {} images".format(dir_path,len(original_files)))
    for path in original_files:
        print(path)
        image = cv2.imread(os.path.join(dir_path, path))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # perform transformations on image
        b = cv2.distanceTransform(image, distanceType=cv2.DIST_L2, maskSize=5)
        g = cv2.distanceTransform(image, distanceType=cv2.DIST_L1, maskSize=5)
        r = cv2.distanceTransform(image, distanceType=cv2.DIST_C, maskSize=5)

        transformed_image = cv2.merge((b, g, r))
        target_file = os.path.join(image_target_dir, path)
        cv2.imwrite(target_file, transformed_image)

if __name__ == '__main__':
    resize = (363, 512)
    main(df_list=['data\\train.csv', 'data\\val.csv'], image_target_dir='data\\')
    #simple_transform(dir_path='original_data\\dataset\\', image_target_dir='predictions\\transformed_2018')