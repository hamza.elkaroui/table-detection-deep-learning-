# Table Boundaries Autodetect
For one of our projects, we sought to use computer vision and machine learning techniques to locate tables in PDF’s 
with a better recall than existing software. 


[Article](https://www.researchgate.net/publication/320243569_Table_Detection_Using_Deep_Learning)


![diagram](src/diagram.png)


## Tabula


## Deep learning with Luminoth


## Requirements
```commandline
pip install tensorflow-gpu
pip install scikitlearn
pip install opencv-python
pip install pandas
pip install Luminoth
```


## Steps: 
### Convert pdf to images
[Pdf to image](https://pdftoimage.com/)


## Create your dataset
[labelImg](https://github.com/tzutalin/labelImg)

### Start an AWS P2/3 instance


## Luminoth

```commandline
lumi checkpoint refresh
lumi checkpoint list
```


## How to use
```commandline
python PascalVOC_to_dataframe.py

python preprocess_image_transformations.py
```

```commandline
lumi dataset transform --type csv --data-dir data/ --output-dir tfdata/ --split train --split val  --only-classes table
```

```commandline
lumi train -c config.yml
```


```commandline
lumi checkpoint create config.yml
```


```commandline
lumi predict --checkpoint 67d10e60cb6b  data\val\Societe-Generale-DDR-2017-depot-amf-08032017-FR-037.jpg
```


```commandline
lumi server web --host 0.0.0.0 --port 8080 --checkpoint 67d10e60cb6b 
```

started at 09:53
finished at 10:53

