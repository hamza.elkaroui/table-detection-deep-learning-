# Research paper : https://www.researchgate.net/publication/320243569_Table_Detection_Using_Deep_Learning?opdsd=1#pf6
# Implementation proposal : https://blog.goodaudience.com/table-detection-using-deep-learning-7182918d778
import click
import os
import sys
from bs4 import BeautifulSoup
import pandas as pd
from sklearn.model_selection import train_test_split



import math
import time
import random


def resize_box(original, new_ , coordonates ):
    original_width, original_height = original
    new_width, new_height = new_
    xmin, ymin, xmax, ymax = coordonates
    width_ratio = new_width / original_width
    height_ratio = new_height / original_height
    return xmin*width_ratio , ymin*height_ratio, xmax*width_ratio, ymax*height_ratio


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

@click.group(context_settings=CONTEXT_SETTINGS)
def cli():
    """A simple command line tool."""
    pass

@cli.command('convert', short_help='Convert bbox files')
@click.argument('directories', nargs=1, type=click.Path(exists=True))
@click.argument('output', nargs=1)
@click.argument('input_format', nargs=1)
@click.argument('output_format', nargs=1)
@click.option('--split', default=None, type=float, help='number of greetings')
@click.option('--resize', nargs=2,  default=None, help='Resize pictures to given size: width height')
def convert(input_format, output_format, directories, output, split, resize):
    """Convert bbox files."""
    print(directories)
    print(output)
    print(split)
    print(resize)
    click.pause()

@cli.group()
def transform():
    """Transform boxes."""

@transform.command('resize', short_help='Resize bbox files.')
def resize():
    """Resize bbox files."""

@transform.command('scale', short_help='Resize bbox files.')
def scale():
    """scale bbox files."""

@transform.command('rotate', short_help='Resize bbox files.')
def rotate():
    """rotate boxes"""


@cli.command('merge', short_help='merge bbox files.')
def merge():
    """merge bbox files."""

@cli.command('plot', short_help='plot bbox files.')
def plot():
    """plot bbox files."""
    click.echo(click.style('I am colored %s' % 'red', fg='green'))
    click.echo(click.style('I am blinking', blink=True))
    click.pause()
    click.clear()




@cli.command('server', short_help='plot bbox files.')
@click.option('--host', default='localhost', type=str, help='number of greetings')
def server(host):
    """plot bbox files."""
    click.launch('http://google.com')











@cli.command()
@click.option('--count', default=8000, type=click.IntRange(1, 100000),
              help='The number of items to process.')
def progress(count):
    """Demonstrates the progress bar."""
    items = range(count)

    def process_slowly(item):
        time.sleep(0.002 * random.random())

    def filter(items):
        for item in items:
            if random.random() > 0.3:
                yield item

    with click.progressbar(items, label='Processing accounts',
                           fill_char=click.style('#', fg='green')) as bar:
        for item in bar:
            process_slowly(item)

    def show_item(item):
        if item is not None:
            return 'Item #%d' % item

    with click.progressbar(filter(items), label='Committing transaction',
                           fill_char=click.style('#', fg='yellow'),
                           item_show_func=show_item) as bar:
        for item in bar:
            process_slowly(item)

    with click.progressbar(length=count, label='Counting',
                           bar_template='%(label)s  %(bar)s | %(info)s',
                           fill_char=click.style(u'█', fg='cyan'),
                           empty_char=' ') as bar:
        for item in bar:
            process_slowly(item)

    with click.progressbar(length=count, width=0, show_percent=False,
                           show_eta=False,
                           fill_char=click.style('#', fg='magenta')) as bar:
        for item in bar:
            process_slowly(item)

    # 'Non-linear progress bar'
    steps = [math.exp( x * 1. / 20) - 1 for x in range(20)]
    count = int(sum(steps))
    with click.progressbar(length=count, show_percent=False,
                           label='Slowing progress bar',
                           fill_char=click.style(u'█', fg='green')) as bar:
        for item in steps:
            time.sleep(item)
            bar.update(item)















def main(directories, output, split, resize):
    if not isinstance(directories, list):
        directories = [directories, ]
    xml_files = [{'directory_labels': directory, 'directory_images': directory.replace('labels', 'images'), 'filename': f} for directory in directories for f in os.listdir(directory) if os.path.isfile( os.path.join(directory, f)) and f.endswith('.xml')]
    flat_records = []
    for file in xml_files:
        f = open(os.path.join(file['directory_labels'], file['filename']), 'r')
        soup = BeautifulSoup(f.read(), 'lxml')
        filename = soup.find(name='filename').text
        size_width = int( soup.find(name='width').text )
        size_height = int( soup.find(name='height').text )
        tables = soup.find_all(name='object')
        for table in tables:
            xmin= int(table.find("xmin").text)
            ymin= int(table.find("ymin").text)
            xmax= int(table.find("xmax").text)
            ymax= int(table.find("ymax").text)
            if resize:
                xmin, ymin, xmax, ymax = resize_box((size_width, size_height),resize,(xmin, ymin, xmax, ymax))
            flat_records.append({
                'directory_labels': file['directory_labels'],
                'directory_images': file['directory_images'],
                'image_id': filename,
                'size_width': size_width,
                'size_height': size_height,
                'xmin': int( table.find("xmin").text ),
                'ymin': int( table.find("ymin").text ),
                'xmax': int( table.find("xmax").text ),
                'ymax': int( table.find("ymax").text ),
                'label': table.find("name").text
            })
    df = pd.DataFrame(flat_records)
    if split and isinstance(split, float):
        train, test = train_test_split(df, test_size=split)
        # train.to_csv(output_path.replace(".csv", "_train.csv"), index=False)
        # test.to_csv(output_path.replace(".csv", "_val.csv"), index=False)

        train.to_csv( os.path.join(output, "train.csv"), index=False)
        test.to_csv( os.path.join(output, "val.csv") , index=False)
    else:
        df.to_csv(output, index=False)

if __name__ == '__main__':
    #cli()
    #resize = (363, 512)
    main(directories=['original_data/dataset', ], output="data/", split=0.2)